# Bootcamp - introduction to Machine Learning
This repository contains notebooks solved as homeworks during the introductory
course to Machine Learning. They cover basic topics, such as: pandas/numpy, 
logistic and linear regression, experiment tracking (clearML).
